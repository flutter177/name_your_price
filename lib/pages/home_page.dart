import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ProductInfo {
  final String name;
  final int price;

  ProductInfo(this.name, this.price);
}

final products = [
  ProductInfo('Wireless Mouse', 3),
  ProductInfo('Keyborad', 5),
  ProductInfo('Mouse', 8),
  ProductInfo('Speaker', 4),
  ProductInfo('iPad', 1000),
];

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  int? _inputtedPrice;
  String _result = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Name Your Price'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(products[_currentIndex].name),
              SizedBox(
                width: 200,
                child: TextField(
                  key: const Key('priceInput'),
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(hintText: "price"),
                  onChanged: (value) {
                    _inputtedPrice = int.tryParse(value);
                  },
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    _result = _inputtedPrice == products[_currentIndex].price
                        ? 'pass'
                        : 'fail';
                  });
                },
                child: const Text('Check'),
              ),
              _result.isNotEmpty
                  ? Text(
                      _result,
                      key: const Key('result'),
                    )
                  : Container(),
              _result.isNotEmpty
                  ? ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _result = '';
                          if (_currentIndex < 4) _currentIndex++;
                        });
                      },
                      child: const Text('Next'),
                    )
                  : Container(),
            ],
          ),
        ));
  }
}
