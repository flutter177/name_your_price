import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:name_your_price/pages/home_page.dart';

void main() {
  group('home page test', () {
    Finder checkBtn() => find.text('Check');
    Finder nextBtn() => find.text('Next');
    Finder priceInput() => find.byKey(const Key('priceInput'));

    testWidgets(
      'change product name after each click',
      (WidgetTester tester) async {
        await tester.pumpWidget(MaterialApp(home: HomePage()));

        expect(find.text(products[0].name), findsOneWidget);

        for (var i = 1; i < 5; i++) {
          await tester.tap(checkBtn());
          await tester.pump();
          await tester.tap(nextBtn());
          await tester.pump();
          expect(find.text(products[i].name), findsOneWidget);
        }
        await tester.tap(checkBtn());
        await tester.pump();
        await tester.tap(nextBtn());
        await tester.pump();
        expect(find.text(products[4].name), findsOneWidget);
      },
    );

    testWidgets('Check result and Next product after each click',
        (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: HomePage(),
      ));

      expect(nextBtn(), findsNothing);
      expect(find.byKey(const Key('result')), findsNothing);

      await tester.enterText(priceInput(), '3');
      await tester.tap(checkBtn());
      await tester.pump();
      expect(find.text('pass'), findsOneWidget);
      expect(nextBtn(), findsOneWidget);
      expect(find.byKey(const Key('result')), findsOneWidget);

      await tester.tap(nextBtn());
      await tester.pump();

      expect(nextBtn(), findsNothing);
      expect(find.byKey(const Key('result')), findsNothing);
      await tester.enterText(priceInput(), '6');
      await tester.tap(checkBtn());
      await tester.pump();

      expect(find.text('fail'), findsOneWidget);
      expect(nextBtn(), findsOneWidget);
      expect(find.byKey(const Key('result')), findsOneWidget);

      await tester.enterText(priceInput(), '5');
      await tester.tap(find.text('Check'));
      await tester.pump();
      expect(find.text('pass'), findsOneWidget);
    });
  });
}
